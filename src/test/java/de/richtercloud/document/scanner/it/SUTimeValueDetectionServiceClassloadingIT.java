/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.it;

import de.richtercloud.document.scanner.valuedetection.sutime.SUTimeValueDetectionService;
import de.richtercloud.document.scanner.valuedetectionservice.ResultFetchingException;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionResult;
import de.richtercloud.document.scanner.valuedetectionservice.ValueDetectionService;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ServiceLoader;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Tests classloading of {@link de.richtercloud.document.scanner.valuedetection.sutime.SUTimeValueDetectionService} at
 * runtime.
 */
public class SUTimeValueDetectionServiceClassloadingIT {

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void testClassloadingAtRuntime() throws MalformedURLException,
            ResultFetchingException {
        URLClassLoader classLoader = new URLClassLoader(new URL[] {Paths.get(System.getProperty("user.home"),
                ".m2",
                "repository",
                "de",
                "richtercloud",
                "document-scanner-valuedetection-sutime",
                "1.0-SNAPSHOT",
                "document-scanner-valuedetection-sutime-1.0-SNAPSHOT-jar-with-dependencies.jar").toUri().toURL()},
                Thread.currentThread().getContextClassLoader()
                    //System.class.getClassLoader doesn't work
        );
        ServiceLoader<ValueDetectionService> serviceLoader = ServiceLoader.load(ValueDetectionService.class,
                classLoader);
        Iterator<ValueDetectionService> serviceLoaderItr = serviceLoader.iterator();
        Set<ValueDetectionService> services = new HashSet<>();
        while(serviceLoaderItr.hasNext()) {
            ValueDetectionService valueDetectionService = serviceLoaderItr.next();
            services.add(valueDetectionService);
        }
        assertEquals(1,
                services.size());
        ValueDetectionService service = services.iterator().next();
        assertTrue(service instanceof SUTimeValueDetectionService);
        List<ValueDetectionResult> valueDetectionResults = service.fetchResults(SimpleDateFormat.getDateInstance(DateFormat.FULL, Locale.getDefault()).format(new Date()),
                ValueDetectionService.retrieveLanguageIdentifier(Locale.getDefault()));
        assertEquals(1,
                valueDetectionResults.size());
    }
}
