/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.it;

import de.richtercloud.document.scanner.gui.DocumentScannerUtils;
import de.richtercloud.document.scanner.model.Company;
import de.richtercloud.document.scanner.model.Document;
import de.richtercloud.document.scanner.model.FinanceAccount;
import de.richtercloud.document.scanner.model.Location;
import de.richtercloud.document.scanner.model.Payment;
import de.richtercloud.reflection.form.builder.jpa.IdGenerationException;
import de.richtercloud.reflection.form.builder.jpa.IdGenerator;
import de.richtercloud.reflection.form.builder.jpa.JPAFieldRetriever;
import de.richtercloud.reflection.form.builder.jpa.MemorySequentialIdGenerator;
import de.richtercloud.reflection.form.builder.jpa.retriever.JPAOrderedCachedFieldRetriever;
import de.richtercloud.reflection.form.builder.jpa.storage.DerbyEmbeddedPersistenceStorage;
import de.richtercloud.reflection.form.builder.jpa.storage.DerbyEmbeddedPersistenceStorageConf;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import de.richtercloud.reflection.form.builder.retriever.FieldOrderValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageConfValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageCreationException;
import de.richtercloud.reflection.form.builder.storage.StorageException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.jscience.economics.money.Currency;
import org.jscience.physics.amount.Amount;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Does what {@code JPAReflectionFormBuilderIT} in
 * {@code reflection-form-builder-it} does, but with de.richtercloud.document.scanner.test.entities of
 * {@code document-scanner} which caused trouble.
 *
 * @author richter
 */
public class DocumentScannerJPAReflectionFormBuilderIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(DocumentScannerJPAReflectionFormBuilderIT.class);

    @Test
    @Ignore //@TODO: onField on ReflectionFormBuilder (subclass) doesn't make sense the onField method should be in a
    // component because it handles an event (which isn't interesting for the builder)
    @SuppressWarnings({"PMD.JUnitTestContainsTooManyAsserts",
        "PMD.CloseResource"})
        //CloseResource: closing resource is unnecessary because try-with-resources would be empty
    public void testOnFieldUpdate() throws IOException,
            StorageCreationException,
            IllegalArgumentException,
            StorageConfValidationException,
            StorageException,
            SQLException,
            FieldOrderValidationException,
            IdGenerationException {
        LOGGER.info("testOnFieldUpdate");
        Set<Class<?>> entityClasses = new HashSet<>(Arrays.asList(Document.class,
                Payment.class));
        File databaseDir = Files.createTempDirectory(DocumentScannerJPAReflectionFormBuilderIT.class.getSimpleName()).toFile();
        FileUtils.forceDelete(databaseDir);
            //databaseDir mustn't exist for Apache Derby
        String databaseName = databaseDir.getAbsolutePath();
        LOGGER.debug(String.format("database directory: %s", databaseName));
        Connection connection = DriverManager.getConnection(String.format("jdbc:derby:%s;create=true", databaseDir.getAbsolutePath()));
        connection.close();
        File schemeChecksumFile = File.createTempFile(DocumentScannerJPAReflectionFormBuilderIT.class.getSimpleName(), null);
        DerbyEmbeddedPersistenceStorageConf storageConf = new DerbyEmbeddedPersistenceStorageConf(entityClasses, databaseName, schemeChecksumFile);
        String persistenceUnitName = "document-scanner-it";
        JPAFieldRetriever fieldRetriever = new JPAOrderedCachedFieldRetriever(DocumentScannerUtils.QUERYABLE_AND_EMBEDDABLE_CLASSES);
        PersistenceStorage<Long> storage = null;
        try {
            storage = new DerbyEmbeddedPersistenceStorage(storageConf,
                    persistenceUnitName,
                    1, //parallelQueryCount
                    fieldRetriever);
            storage.start();
            IdGenerator<Long> idGenerator = MemorySequentialIdGenerator.getInstance();

            //entity setup
            Location location = new Location("description");
            Company sender = new Company("name",
                    new LinkedList<>(),
                    new LinkedList<>(),
                    new LinkedList<>(),
                    new LinkedList<>());
            Company recipient = new Company("name",
                    new LinkedList<>(),
                    new LinkedList<>(),
                    new LinkedList<>(),
                    new LinkedList<>());
            FinanceAccount senderAccount = new FinanceAccount("1",
                    "2",
                    "3",
                    "4",
                    new LinkedList<>(),
                    sender);
            FinanceAccount recipientAccount = new FinanceAccount("5",
                    "6",
                    "7",
                    "8",
                    new LinkedList<>(),
                    recipient);

            //without mappedBy
            //test many-to-many
            Document entityA1 = new Document("comment",
                    "identifier",
                    new Date(),
                    new Date(),
                    location,
                    false,
                    false,
                    sender,
                    recipient);
            Payment entityB1 = new Payment(Amount.valueOf(0, Currency.EUR),
                    new Date(),
                    senderAccount,
                    recipientAccount);
            Payment entityB2 = new Payment(Amount.valueOf(0, Currency.EUR),
                    new Date(),
                    senderAccount,
                    recipientAccount);
            location.setId(idGenerator.getNextId(location));
            sender.setId(idGenerator.getNextId(sender));
            recipient.setId(idGenerator.getNextId(recipient));
            senderAccount.setId(idGenerator.getNextId(senderAccount));
            recipientAccount.setId(idGenerator.getNextId(recipientAccount));
            entityB1.setId(idGenerator.getNextId(entityB1));
            entityB2.setId(idGenerator.getNextId(entityB2));
            entityA1.setId(idGenerator.getNextId(entityA1));
            storage.store(location);
            storage.store(sender);
            storage.store(recipient);
            storage.store(senderAccount);
            storage.store(recipientAccount);
            storage.store(entityB1);
            storage.store(entityB2);
            storage.store(entityA1); //only store entityA1
            Document entityA1Stored = storage.retrieve(entityA1.getId(),
                    Document.class);
            Payment entityB1Stored = storage.retrieve(entityB1.getId(),
                    Payment.class);
            Payment entityB2Stored = storage.retrieve(entityB2.getId(),
                    Payment.class);
            assertTrue(entityA1Stored.getPayments().contains(entityB1));
            assertTrue(entityA1Stored.getPayments().contains(entityB2));
            assertTrue(entityB1Stored.getDocuments().contains(entityA1));
            assertTrue(entityB2Stored.getDocuments().contains(entityA1));
        }finally {
            if(storage != null) {
                storage.shutdown();
            }
        }
    }
}
